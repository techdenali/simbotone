/*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */

/* Start Scrapper script..this is for each retailer different, have factory pattern */

var simonScrapperDAO=require('../dao/SimonScrapperDAO.js');
var simonScrapperUtility=require('../utility/SimonScrapperUtility.js');
var async=require('async');
//var promise = require('promise');
/*
function callEachScrapper()
{
	simonScrapperDAO.getOrders(function(orders){		
		async.forEachOf(orders, func(order){
		        //simonScrapperUtility.matchSellerName(order, callback);
		        //callback();
		    },function(err){
		    	console.log("Something fishy happened"+err);
		    });		
	});
	console.log("service");
}*/

async function callEachScrapper(){ 
	await simonScrapperDAO.getOrders(function(orders){
		
		for(var index=0;index<orders.length;index++)
		{
			
		//console.log(orders[index]);
		console.log('In scraper Service\n');
		if(orders[index].flag=='0')
			{
				//simonScrapperUtility.matchSellerName(orders[index]);
				simonScrapperUtility.matchSellerName(orders[index]);
			    
			/*require('chromedriver');
			var webdriver = require('selenium-webdriver'),
			By = require('selenium-webdriver').By,
			until = require('selenium-webdriver').until,
			Key = require('selenium-webdriver').Key,
			Builder=require('selenium-webdriver').Builder; 
			//var promise = require('promise');

			var chrome=require('selenium-webdriver/chrome');
			var options = new chrome.Options();
			options.addArguments("headless:false");

			//var driver = new Builder().forBrowser('chrome').withCapabilities(options.toCapabilities()).build();
			var driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();

			driver.get('https://www.lastcall.com/assistance/assistance.jsp');//.then(function (){
				driver.findElement(By.name('Id')).clear();
				driver.getTitle().then(function(title) {
	                console.log(title, "Scot Logic Blog");
	            });

			    driver.findElement(By.name('Id')).sendKeys(orders[index].retailer_order_id);    //giving  retailer_order_id over here
			    driver.findElement(By.name('Email')).clear();
				driver.findElement(By.name('Email')).sendKeys(orders[index].email_id);          //giving  email_id over here
				driver.findElement(By.id('checkFlag')).click();
			//});
			driver.quit();
*/		}
		}
		
		
		/*orders.forEach(function(order){
			asyncTasks.push(function(callback){
				simonScrapperUtility.matchSellerName(order,function(callback){
					callback();
				});
			});
		});
		
		asyncTasks.push(function(callback){
			setTimeout(function(){
				callback();}, 10000);
		});
		
		async.parallel(asyncTasks, function(){
			console.log('Hopefully done');
		});*/
		/*//async.forEach(orders, function (item, callback){ 
		orders.forEach(){
		//async.forEach(orders, simonScrapperUtility.matchSellerName(order,function(){ 
       console.log("iteration"); // print the key
			simonScrapperUtility.matchSellerName(item);
        // tell async that that particular element of the iterator is done
        callback(); 

        }, function(err) { 
        console.log('iterating done');
      });*/
	});
}
//console.log(orders.length);
//for(var index=0;index<orders.length;index++)
//	{
////	var order;
//	//console.log(orders[index]);
//		if(orders[index].flag=='0')
//			{
//				simonScrapperUtility.matchSellerName(orders[index]);
//			}
//	}

module.exports.callEachScrapper=callEachScrapper;