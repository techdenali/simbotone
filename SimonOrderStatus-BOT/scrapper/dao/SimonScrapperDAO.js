/*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */

/* Make database entries if successful scrapper

 */
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/Order_BOT";	
function getOrders(orders)
{
	 console.log('getOrders inside Orders Dao');
	
	MongoClient.connect(url, function(err, db) {
		console.log('getOrders inside Orders connect');
	  if (err) throw err;
	 
	  db.collection("order",null,function (err,callback){
		  console.log('getOrders inside Orders callback');
		  if (err) throw err;
		 // console.log(callback);
		  callback.find().toArray(function (err,result){
			  console.log('getOrders inside Orders result');
		  if (err) throw err;
		//  console.log(result);
		  db.collection("order").find().toArray(function (err,callback){
			  
			 console.log( callback);
			 db.close();
			  orders(callback);
		  });
		  
		  
		 
	  });
	  });
	  
	 // console.log(allProductsArray[1]);
		  
	  
		  
	 
	 /* .forEach(function(result) {
		  console.log('getOrders inside Orders collection');
	   // if (err) throw err;
	   console.log(result);
	    //orders(result);
	   newResult=result;*/
	  //  db.close();
	//  });
	  
	 // orders(newResult);
	});
}

async function getSellerName(sellerId,seller)
{
	MongoClient.connect(url, function(err, db) {
		  if (err) throw err;
		  //db.collection("seller_info").find({"seller_id":sellerId}).toArray(function(err, result) {
		  db.collection("seller_info").findOne({"seller-id":sellerId},function(err, result) {
		    if (err) throw err;
		    seller(result);
		    db.close();
		 
		  });
		  
	});
}
module.exports.getOrders=getOrders;
module.exports.getSellerName=getSellerName;