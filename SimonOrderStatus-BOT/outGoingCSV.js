/**
 * http://usejsdoc.org/
 */
function outGoingCSV()
{
	var MongoClient = require('mongodb').MongoClient;
	var url = 'mongodb://localhost:27017/Order_BOT';
	MongoClient.connect(url, function(err, db) {
	if (err) throw err;

	var dbs=db.db("Order_BOT");
	var cursor = dbs.collection('scrap').find({}, {_id: false});
   
	var fs=require('fs');
	var csv=require('fast-csv');

	var csvStream = csv.createWriteStream({headers:['seller-id','order-date','retailer-order-id','SPO-order-id','SPO-order-line-id','product-id','order-status','tracking-number','carrier-url','product-amount','product-name','shipping-amount','tax-amount','product-price','product-color','product-size']}),
    writableStream = fs.createWriteStream("outgoingCSV.csv");
 
  
	cursor.forEach(function(doc) {
		
	
	writableStream.on("finish", function(){
				//console.log("DONE!");
		});
		csvStream.write(doc);
   
		});
		csvStream.pipe(writableStream); 
   
		db.close();
		deleteScrap();
		});
}
function deleteScrap()
{
	var MongoClient = require('mongodb').MongoClient;
	var url = "mongodb://localhost:27017/";

	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var dbo = db.db("Order_BOT");
	  dbo.collection("scrap").drop(function(err, delScrap) {
	    if (err) throw err;
	    if (delScrap) 
	    	console.log("Collection deleted");
	    db.close();
	  });
	});
}
module.exports.outGoingCSV= outGoingCSV;