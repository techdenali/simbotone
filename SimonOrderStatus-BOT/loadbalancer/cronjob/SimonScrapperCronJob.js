/*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */

/* Check if any new orders to scrap in database, runs as per retailer, or all retailers on time in configuration or manually, should also get interrupts to stop loadbalance or exit gracefully, invoke manually*/
