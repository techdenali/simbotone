/*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */
 

 /**
 * http://usejsdoc.org/
 */

var hapi=require('hapi');
var fs = require('fs');
var nconf = require('nconf');


nconf.file({ file: 'config/development/project_config.json' });

var port=nconf.get('app:port');

//Init Sever
const server = new hapi.Server({ 
	port: port, host: '0.0.0.0'
});


server.route({
    method: 'GET',
    path: '/Orders',
    handler: (request, reply) => {
    	startTime();
        return 'Orders BOT Started';
    }
});


server.route({
    method: 'GET',
    path: '/scrap',
    handler: (request, reply) => {
    	var scrapper=require('./scrapper/service/SimonScrapperService.js');
    	scrapper.callEachScrapper();
        return 'Scraping Started';
    }
});


server.route({
    method: 'GET',
    path: '/csv',
    handler: (request, reply) => {
    	var outGoingCSV = require('./outGoingCSV.js');
    	outGoingCSV.outGoingCSV();
    	
    	//startTime();
    	console.log('CSV Generated');
        return 'csv generated ';
    }
}); 

server.start();

console.log('Server running at:', server.info.uri);



function startTime()
{
	var fileName='SimonCronFacade';
	var cronFacadeFile=require('./server/facade/'+fileName);
	var date = new Date();
	cronFacadeFile.csvCronJob(date);
}


//csvCronJob(); //running the cron job

/*function csvCronJob()
{
 //console.log('the hapi console calling   :: ');

var cronJob = require('cron').CronJob;  
var time=nconf.get('cronJobTime');
try {
var job = new cronJob({ 
    cronTime:time, 
    onTick: function(){
    	var date = new Date();
    	simonCronFacade.csvCronJob(date);
    }
    });
job.start();
}
catch(ex) {
    console.log("cron pattern not valid");
}
 
}*/


