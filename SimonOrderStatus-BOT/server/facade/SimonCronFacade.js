/*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */
 
/* Validate excepton scenarios like, what will happen if 2 or more cvs files are there in ftp location? How to handle errors/ error handling here.
 */
var simonCronpopulator=require('../populator/SimonPopulator.js');
var simonCronService=require('../service/SimonCronService.js');
function csvCronJob(date)
{
console.log(date);

 simonCronpopulator.readCsv(function(jsonArray){
	 //console.log(jsonArray);
	// console.log('facade');
	 simonCronService.jsonCsv(jsonArray);
 });
 
}

module.exports.csvCronJob=csvCronJob;