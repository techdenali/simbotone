/*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */
 
/* Save in data cron file start and end time along with results, new entry per cron job
Make entries in database (in batch size) to indicate status of all orders to scrap coming from file, consignment table */
/*jshint esversion: 6 */
var async = require('async');

function dumy(callback){
	console.log('dumy');
	callback();
	
}

function jsonCsvDao(jsonArray)
{
	console.log(jsonArray.length+'This is the incoming json');
	console.log('This is my dao');
	
	var MongoClient = require('mongodb').MongoClient;
	var url = "mongodb://localhost:27017/Order_BOT";

	MongoClient.connect(url, function x(err, db) {
	  if (err) throw err;
	  var i;
	  for(i=0;i<jsonArray.length;i++) {
		  var json=jsonArray[i];
		  
		  db.collection('order').insert(json, function y(err, res) {
			  if(err) throw err;
			//console.log(res);
			   // console.log(i);	
			  }); //db
		  
	  }
	  db.close();
	  //callback(jsonArray);
	  console.log(jsonArray.length);
	  var simonScrapperFacade=require('../../scrapper/facade/SimonScrapperFacade.js');
	  simonScrapperFacade.callScrapper();
	  
	});
	
	
}


/*async.waterfall([jsonCsvDao,dumy]  , ()=> {
	 console.log('done');
	 var simonScrapperFacade=require('../../scrapper/facade/SimonScrapperFacade.js');
	  simonScrapperFacade.callScrapper();
    
   });*/

module.exports.jsonCsvDao=jsonCsvDao;