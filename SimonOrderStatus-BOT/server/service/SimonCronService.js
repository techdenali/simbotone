/*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */
 
/* process cron audit details via dao, if no CSV then exit cron job gracefully.
 */
var simonCronDAO=require('../dao/SimonCronDAO.js');
function jsonCsv(jsonArray)
{
	simonCronDAO.jsonCsvDao(jsonArray);
}


module.exports.jsonCsv=jsonCsv;