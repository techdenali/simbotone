/*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */
 
/* read CSV file, make csv validation checks.
 */
//var simonCronService=require('../service/SimonCronService.js');

var jsonArray=[];
var csvRows;
function readCsv(jsonArray)
{
	
var csv = require('node-csv').createParser();
const csvFilePath = '../SimonOrderStatus-BOT/incomingcsv.csv';
csv.mapFile(csvFilePath, function(err,data){
	csvRows=data;
	validateCsv(function(result) {
		jsonArray(result);
		//console.log(result);
	}
			
	);
});
//console.log(rows);
}
//readCsv();



function validateCsv(done)
{	
	
	
	var nconf = require('nconf');
	nconf.file({file:'../SimonOrderStatus-BOT/config/development/retailer_config.json'});
	
	//var flag;	
	
	
	for(var i=0;i<csvRows.length;i++)
		{
		if(csvRows[i]["seller-id"]!=='')
			{
				var temp=nconf.get('Mandatoryfields:'+csvRows[i]["seller-id"]);
			}
		
		var mandatoryFields=temp.split(',');
			var flag=0;
			for(var a in csvRows[i])
			{
				for(var j=0;j<mandatoryFields.length;j++)
					{
						if (a == mandatoryFields[j])
						{
							//flag=1;
							if(csvRows[i][a]!=='')
							{
							flag++;
							break;	
							}
						}
					}
					if(flag==mandatoryFields.length)
					{
						csvRows[i].flag='0';
						//console.log(csvRows[i]);
							jsonArray.push(csvRows[i]);
							flag=0;
					}				
			}
			
		}
	//console.log(jsonArray);
	//return jsonArray;
	done(jsonArray);
	//simonCronService.jsonCsv(jsonArray);
}


module.exports.readCsv=readCsv;