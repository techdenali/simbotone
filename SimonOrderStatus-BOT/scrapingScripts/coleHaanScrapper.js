 /*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */ 

    var ChOrderDetails={};

    //setup driver
    require('chromedriver');
	var webdriver = require('selenium-webdriver'),
	By = require('selenium-webdriver').By;
	until = require('selenium-webdriver').until,
	Key = require('selenium-webdriver').Key;
	Builder=require('selenium-webdriver').Builder; 
	var chrome=require('selenium-webdriver/chrome');
	var options = new chrome.Options();
	options.addArguments("headless");
	

	
	async   function scrap(order){
    	
    var driver = new Builder().forBrowser('chrome').withCapabilities(options.toCapabilities()).build();
	//navigate to Cole Haan	
    await driver.get('https://www.colehaan.com/on/demandware.store/Sites-ColeHaan_US-Site/en_US/Order-History?OrderLookup=true');
	
	// Fill Details
	await driver.findElement(By.xpath('/html/body/div[5]/div[1]/button/span[2]')).click();
	await driver.findElement(By.name('dwfrm_ordertrack_orderNumber')).clear();
	await driver.findElement(By.name('dwfrm_ordertrack_orderNumber')).sendKeys(order["retailer-order-id"]);
	await driver.findElement(By.name('dwfrm_ordertrack_postalCode')).clear();
	await driver.findElement(By.name('dwfrm_ordertrack_postalCode')).sendKeys(order["zip-code"]);
	await driver.findElement(By.name('dwfrm_ordertrack_emailAddress')).clear();
	await driver.findElement(By.name('dwfrm_ordertrack_emailAddress')).sendKeys(order["email-id"]+ Key.ENTER);
   
var dateFormat = require('dateformat');	
	//order date
	await driver.findElement(By.xpath('//*[@id="primary"]/div[2]/div[2]/table/tbody/tr[2]/td[1]/div[1]/span[2]')).getText().then(function(order_date){
		console.log('')
		            ChOrderDetails["order-date"] = dateFormat(order["order-date"],"yyyy-mm-dd");
					
	});
	
	//retailer order id	
	await driver.findElement(By.xpath('//*[@id="primary"]/div[2]/div[2]/table/tbody/tr[2]/td[1]/div[2]/span[2]')).getText().then(function(retailer_order_id){
		            ChOrderDetails["retailer-order-id"] = retailer_order_id;

	});
	
	
	//shipping amount
	await driver.findElement(By.xpath('//*[@id="primary"]/div[2]/div[2]/table/tbody/tr[2]/td[4]/div/table/tbody/tr[2]/td[2]/span')).getText().then(function(shipping_amount){
		            ChOrderDetails["shipping-amount"] = shipping_amount.slice(1);
					
	});
	//tax amount
	await driver.findElement(By.xpath('//*[@id="primary"]/div[2]/div[2]/table/tbody/tr[2]/td[4]/div/table/tbody/tr[3]/td[2]')).getText().then(function(tax_amount){
		            ChOrderDetails["tax-amount"] = tax_amount.slice(1);
				
	});
	
	//order_status in product table
	await driver.findElement(By.xpath('//*[@id="primary"]/div[3]/table/tbody/tr/td[5]/span[2]/a')).getText().then(function(order_status){
		ChOrderDetails["order-status"]=order_status.substr(6);

				    if(ChOrderDetails["order-status"]==='Shipped'){
				    	
					//tracking_number
					    driver.findElement(By.xpath('//*[@id="primary"]/div[2]/div[2]/table/tbody/tr[2]/td[1]/div[3]/span[2]/a')).getText().then(function(tracking_number){
					     ChOrderDetails["tracking-number"]=tracking_number;
					 
				        });
				 }//if
	});//order_status in product table
	 
	
	 //carrier_url
	await driver.findElement(By.xpath('//*[@id="primary"]/div[3]/table/tbody/tr/td[5]/span[2]/a')).getAttribute('href').then(function(carrier_url){
		     ChOrderDetails["carrier-url"]=carrier_url;
		   
	 });
	 //product id
	await driver.findElement(By.xpath('//*[@id="primary"]/div[3]/table/tbody/tr/td[2]/div[1]/div[4]/span[2]')).getText().then(function(product_id){
				ChOrderDetails["_id"] = product_id;
		ChOrderDetails["product-id"] = product_id;
	}); 
	//product name
	await driver.findElement(By.xpath('//*[@id="primary"]/div[3]/table/tbody/tr/td[2]/div[1]/div[1]/a')).getText().then(function(product_name){
		ChOrderDetails["product-name"] = product_name;
		//console.log(productData.product_name);
	});
	//product amount
	await driver.findElement(By.xpath('//*[@id="primary"]/div[3]/table/tbody/tr/td[4]')).getText().then(function(product_amount){
	    
		ChOrderDetails["product-amount"] = product_amount.slice(1);
		ChOrderDetails["seller-id"]=order["seller-id"];
		ChOrderDetails["SPO-order-id"]=order["SPO-order-id"];
		ChOrderDetails["SPO-order-line-id"]=order["SPO-order-line-id"];
		ChOrderDetails["product-id"]=order["product-id"];
	    //Saving to Database
		insertDocument(ChOrderDetails);
	});
	await driver.close();
}//scrap function ends here




//saving of scrapping details starts from here
function insertDocument(ChOrderDetails)
{
	
	var MongoClient = require('mongodb').MongoClient;
	var url = "mongodb://localhost:27017/";	
		MongoClient.connect(url, function(err, db) {
			if (err) {
				console.log('problem while connection to server'+err);
			}
			
			var dbs=db.db('Order_BOT');
			
					dbs.collection('scrap').insertOne(ChOrderDetails,function(err,response){
						if(err) {
							console.log('problem while inserting data to db'+err);
						}
						console.log('inserted into db');
					}); 
				
				db.close();	
       });	
}//insertDocument


module.exports.scrap=scrap;