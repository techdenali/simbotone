 /*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */ 

//global variables    
var jcrewOrderDetails={};
var productCount={};
var dateFormat = require('dateformat');		

    require('chromedriver');
	var webdriver = require('selenium-webdriver'),
    By = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Key = require('selenium-webdriver').Key,
    Builder=require('selenium-webdriver').Builder; 
    var chrome=require('selenium-webdriver/chrome');
	var options = new chrome.Options();
	options.addArguments("headless:false");

async function scrap(order){
	var driver = new Builder().forBrowser('chrome').withCapabilities(options.toCapabilities()).build();
	await driver.get("https://factory.jcrew.com/help/order_status.jsp?sidecar=true");
    //enter details
	await driver.findElement(By.name("ORDER_SEARCH<>orderNum_search")).sendKeys(order["retailer-order-id"]);
	await driver.findElement(By.name("ORDER_SEARCH<>postal")).sendKeys(order["zip-code"] + Key.ENTER);
	await driver.findElement(By.xpath('/html/body/div[2]/div[3]/form/table/tbody/tr[5]/td/table/tbody/tr[1]/td/table[2]')).findElements(By.css('tr')).then(function(elements){
			 	  var countOfRow=elements.length;
			 	 productCount.count=countOfRow;
			 	 scrapingProducts(productCount,order,driver);
			 	jcrewOrderDetails={};
			 	
             });  
}//scrap function ends here
async function scrapingProducts(productCount,order,driver){
		for(var j=2;j<productCount.count;j++){
		    await driver.findElement(By.xpath('/html/body/div[2]/div[3]/form/table/tbody/tr[5]/td/table/tbody/tr[1]/td/table[1]/tbody/tr/td/b')).getText().then(function(retailer_order_id){
			jcrewOrderDetails["retailer-order-id"] = retailer_order_id.substr(7);
            });
		
		    await driver.findElement(By.xpath('/html/body/div[2]/div[3]/form/table/tbody/tr[4]/td/table/tbody/tr/td/table/tbody/tr[5]/td[3]')).getText().then(function(shipping_amount){
			jcrewOrderDetails["shipping-amount"]=shipping_amount;
			});
		    await driver.findElement(By.xpath('/html/body/div[2]/div[3]/form/table/tbody/tr[4]/td/table/tbody/tr/td/table/tbody/tr[6]/td[3]')).getText().then(function(tax_amount){
			jcrewOrderDetails["tax-amount"]=tax_amount;
			});
			await driver.findElement(By.xpath('/html/body/div[2]/div[3]/form/table/tbody/tr[4]/td/table/tbody/tr/td/table/tbody/tr[11]/td[3]')).getText().then(function(total){
			});
			await driver.findElement(By.xpath('/html/body/div[2]/div[3]/form/table/tbody/tr[5]/td/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td[19]')).getText().then(function(order_status){
				jcrewOrderDetails["order-status"] = order_status;
							
				if(jcrewOrderDetails["order-status"]==='Shipped'|| jcrewOrderDetails["order-status"].includes('Returned')){
					
					 driver.findElement(By.xpath('/html/body/div[2]/div[3]/form/table/tbody/tr[5]/td/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td[23]')).getText().then(function (tracking_number) {
						jcrewOrderDetails["tracking-number"] = tracking_number;
					 });
					 driver.findElement(By.xpath('/html/body/div[2]/div[3]/form/table/tbody/tr[5]/td/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td[23]/a')).getAttribute('href').then(function (carrier_url) {
						jcrewOrderDetails["carrier-url"] = carrier_url;
					 });			
				}//if ends
				else{
					jcrewOrderDetails["tracking-number"] = "";
					jcrewOrderDetails["carrier-url"] ="";
				}//else ends
				
			 });
			 await driver.findElement(By.xpath('/html/body/div[2]/div[3]/form/table/tbody/tr[5]/td/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td[15]')).getText().then(function(product_amount){
				jcrewOrderDetails["product-amount"] = product_amount.slice(1);
				
				if((jcrewOrderDetails["order-status"].includes('Cancelled')||jcrewOrderDetails["order-status"].includes('Shipped')||jcrewOrderDetails["order-status"].includes('Returned'))){
					jcrewOrderDetails["order-date"]=dateFormat(order["order-date"],"yyyy-mm-dd");
						jcrewOrderDetails["seller-id"]=order["seller-id"];
						jcrewOrderDetails["SPO-order-id"]=order["SPO-order-id"];
						jcrewOrderDetails["SPO-order-line-id"]=order["SPO-order-line-id"];
						jcrewOrderDetails["product-id"]=order["product-id"];
						jcrewOrderDetails["product-name"]='';
						
					//saving to DB
				
					insertDocument(jcrewOrderDetails);
					}
				jcrewOrderDetails={};
			});
			
		}//loop ends here
		 await driver.close();
	}

function insertDocument(jcrewOrderDetails)
{
	
	var MongoClient = require('mongodb').MongoClient;
	var url = "mongodb://localhost:27017/";	
		MongoClient.connect(url, function(err, db) {
			if (err) {
				console.log('problem while connection to server'+err);
			}
		
			var dbs=db.db('Order_BOT');
					dbs.collection('scrap').insertOne(jcrewOrderDetails,function(err,response){
						if(err) {
							console.log('problem while inserting data to db'+err);
						}
						console.log('inserted into db');
					}); 
       });	
	}

module.exports.scrap=scrap;