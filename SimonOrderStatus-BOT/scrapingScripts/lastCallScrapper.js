/*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */
var LcOrderDetails={};
var productCount={};

//driver setup
require('chromedriver');
var webdriver = require('selenium-webdriver'),
By = require('selenium-webdriver').By,
until = require('selenium-webdriver').until,
Key = require('selenium-webdriver').Key,
Builder=require('selenium-webdriver').Builder; 
var chrome=require('selenium-webdriver/chrome');
var options = new chrome.Options();
options.addArguments("headless:false");

/* scrap() gets invoked by SimonScrapperUtility.matchSellerName(), 
 * takes single order each time with mandatory fields and
 * returns productCount which is there in the products page
 * that will pass to scrapingProducts() along with driver instance and incoming orders.
 */
async function scrap(order){	
	//chrome driver instance is created
	var driver = new Builder().forBrowser('chrome').withCapabilities(options.toCapabilities()).build();
	//Entering order Details
	await driver.wait(driver.get('https://www.lastcall.com/assistance/assistance.jsp'),10000);
	await driver.findElement(By.name('Id')).clear();
	await driver.findElement(By.name('Id')).sendKeys(order["retailer-order-id"]);    
	await driver.findElement(By.name('Email')).clear();
	await driver.findElement(By.name('Email')).sendKeys(order["email-id"]);          
	await driver.findElement(By.id('checkFlag')).click();
	//getting row count in the Html page.  			
	await driver.findElement(By.xpath('//table[contains(@class,"account-table")]/tbody')).findElements(By.className('primary-view')).then(function(elements){
		 	  var countOfRow=elements.length;
		 	 productCount.count=countOfRow;
		 
		 //calling  this function  with a parameter productCount in an order
		  scrapingProducts(productCount,order, driver);		
     });
}
/* Invoked by scrap()
 * this function is for different products in an order,
 * returns one productDetails in the json Object format, 
 * will send that Object to insertDocument() 
 * to persist the scrapped product data
 * */ 
async function scrapingProducts(productCount,order, driver){

	  for(var i=2;i<productCount.count;i++){
		
        var dateFormat = require('dateformat');	
        //getting
		await driver.findElement(By.xpath('//*[@id="cancelLine"]/table/tbody/tr[1]/td[2]/div[1]/span[2]')).getText().then(function(order_date){
				LcOrderDetails["order-date"]=dateFormat(order["order-date"],"yyyy-mm-dd");	
		});
		
		//this is for scraping retailer_order_id from the retailer_page
		await driver.findElement(By.xpath('//*[@id="cancelLine"]/table/tbody/tr[1]/td[2]/div[2]/a')).getText().then(function(retailer_order_id){
			LcOrderDetails["retailer-order-id"]=retailer_order_id;
			//LcOrderDetails.ship_date = "";
		});

		//this is for scraping product_name from the retailer_page
		await driver.findElement(By.xpath('//*[@id="cancelLine"]/table/tbody/tr['+i+']/td[2]/span[1]')).getText().then(function(product_name){
			LcOrderDetails["product-name"] = product_name;
		});
	
		//this is for scraping order_status from the retailer_page
		await driver.findElement(By.xpath('(//*[@id="cancelLine"]/table/tbody/tr['+i+']/td[3])')).getText().then(function(order_status){
			
			if(order_status.includes('Cancelled')){   
				LcOrderDetails["order-status"] = order_status;  
				LcOrderDetails["tracking-number"]="";
				LcOrderDetails["carrier-url"]="";
			}
			
			//If order_status other than Cancelled means it will have carrier_url and tracking_number
			else{	
					var statusArray=order_status.split("\n");	                        
					var status=statusArray[0];
					var tracking_number=statusArray[1].substr(12);
					LcOrderDetails["order-status"] = status;
					LcOrderDetails["tracking-number"]=tracking_number;	
					
						driver.findElement(By.xpath('//div[@class="statusTracking"]/span/a')).getAttribute('href').then(function (carrier_url) {
						LcOrderDetails["carrier-url"] = carrier_url;
						
				 });	
			 } 
						
		 });
		
		//this is for scraping product_amount from the retailer_page
		await driver.findElement(By.xpath('//*[@id="cancelLine"]/table/tbody/tr['+i+']/td[4]')).getText().then(function(product_amount){
			if(product_amount.includes('x $'))
			{	
			product_amount=product_amount.split("x $");
			product_amount=product_amount[0]*product_amount[1];
			LcOrderDetails["product-amount"] = product_amount;
			}
			else{
				product_amount=product_amount.split("x -$");
				product_amount=(-1)*product_amount[0]*product_amount[1];
				LcOrderDetails["product-amount"] = product_amount;
			}
		});	
		
		//this is for scraping shipping_amount from the retailer_page
		await driver.findElement(By.xpath('//*[@id="cancelLine"]/table/tbody/tr['+productCount.count+']/td/dl/dd[2]')).getText().then(function(shipping_amount){
			LcOrderDetails["shipping-amount"] = shipping_amount.slice(1);
			
		});
		
		//this is for scraping tax_amount from the retailer_page
		await driver.findElement(By.xpath('//*[@id="cancelLine"]/table/tbody/tr['+productCount.count+']/td/dl/dd[3]')).getText().then(function(tax_amount){
			LcOrderDetails["tax-amount"] = tax_amount.slice(1);
				
			});
		
		//this is for scraping grand_total from the retailer_page
		await driver.findElement(By.xpath('//*[@id="cancelLine"]/table/tbody/tr[1]/td[4]')).getText().then(function(grand_total){
				if((LcOrderDetails["order-status"].includes('Cancelled')||LcOrderDetails["order-status"].includes('Shipped')||LcOrderDetails["order-status"].includes('Returned'))){
					if(LcOrderDetails["product-name"].trim()==order["product-name"].trim()){
						LcOrderDetails["seller-id"]=order["seller-id"];
						LcOrderDetails["SPO-order-id"]=order["SPO-order-id"];
						LcOrderDetails["SPO-order-line-id"]=order["SPO-order-line-id"];
						LcOrderDetails["product-id"]=order["product-id"];
						
						insertDocument(LcOrderDetails);
					}
					else{
						console.log('\tnot inserted into db');
					    }
					}
			
				LcOrderDetails={};
		});
		 
		}//loop end here
	await driver.close();
}//function scrapingProducts ends here



//saving of scrapping details starts from here
function insertDocument(LcOrderDetails)
{
	
	var MongoClient = require('mongodb').MongoClient;
	var url = "mongodb://localhost:27017/";	
		MongoClient.connect(url, function(err, db) {
			if (err) {
				console.log('problem while connection to server'+err);
			         }
			var dbs=db.db('Order_BOT');
					dbs.collection('scrap').insertOne(LcOrderDetails,function(err,response){
						if(err) {
							console.log('problem while inserting data to db'+err);
						}
						console.log('inserted into db');
					}); 
				
				db.close();	
       });	
	}

module.exports.scrap=scrap;