 /*
  Copyright (c) 2016-2018 Tech Denali LLC.
  All Rights Reserved. 
  No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission.
  Please see the LICENSE file included with   this distribution for details. 
 */ 

//global variabless
var off5thOrderDetails={};

require('chromedriver');
var webdriver = require('selenium-webdriver'),
By = require('selenium-webdriver').By,
until = require('selenium-webdriver').until,
Key = require('selenium-webdriver').Key,
Builder=require('selenium-webdriver').Builder; 
var chrome=require('selenium-webdriver/chrome');
var options = new chrome.Options();
options.addArguments("headless:false");


async function scrap(order){

	var driver = new Builder().forBrowser('chrome').withCapabilities(options.toCapabilities()).build();
//Open the Homepage
await driver.get('https://www.saksoff5th.com/account/login?_k=%2Faccount%2Forder-history');


//Fill Details
await driver.findElement(By.name('order_num')).clear();
await driver.findElement(By.name('order_num')).sendKeys(order["retailer-order-id"]);
await driver.findElement(By.name('billing_zip_code')).clear();
await driver.findElement(By.name('billing_zip_code')).sendKeys(order["zip-code"] + Key.ENTER);

//Wait for Order Details page to be loaded
await driver.wait(until.elementLocated(By.xpath('//div[@class="order-history-order__header-order-num"]')), 10000);

var dateFormat = require('dateformat');await driver.findElement(By.xpath('//div[@class="order-history-order__header-order-num"]')).getText().then(function(retailer_order_id){
	
	off5thOrderDetails["seller-id"]=order["seller-id"];
	off5thOrderDetails["order-date"]=dateFormat(order["order-date"],"yyyy-mm-dd");
	off5thOrderDetails["SPO-order-id"]=order["SPO-order-id"];
	off5thOrderDetails["SPO-order-line-id"]=order["SPO-order-line-id"];
	off5thOrderDetails["product-id"]=order["product-id"];
	off5thOrderDetails["retailer-order-id"]=retailer_order_id.substr(7);
});
await driver.findElement(By.xpath('//span[@class="order-billing-details__content" and .="Shipping"]/following-sibling::span/span')).getText().then(function(shipping_amount){
	off5thOrderDetails["shipping-amount"]=shipping_amount.slice(1);
});
await driver.findElement(By.xpath('//span[@class="order-billing-details__content" and .="Tax"]/following-sibling::span/span')).getText().then(function(tax_amount){
	off5thOrderDetails["tax-amount"]=tax_amount.slice(1);
});
await driver.findElement(By.xpath('//a[contains(@class, "order-history-item__link")]')).getText().then(function (product_name) {
	off5thOrderDetails["product-name"]=product_name;
});
await driver.findElement(By.xpath('//div[contains(@class, "order-history-item__price-column")]//p[contains(@class, "order-history-item__content")]/span')).getText().then(function (product_amount) {
	off5thOrderDetails["product-amount"]=product_amount.slice(1);
});
await driver.findElement(By.xpath('//div[contains(@class, "order-history-item__status-column")]//p[contains(@class, "order-history-item__content")]')).getText().then(function (order_status) {
	off5thOrderDetails["order-status"]=order_status;
	if(order_status.includes('Cancelled')){   
		
		off5thOrderDetails["order-status"] = order_status;  
		off5thOrderDetails["tracking-number"]="";
		off5thOrderDetails["carrier-url"]="";
	}
	else{
		
		driver.findElement(By.xpath('//div[contains(@class, "order-history-item__status-column")]//a[.="Track Item"]')).getAttribute('href').then(function (carrier_url) {
			off5thOrderDetails["tracking-number"]="";
			off5thOrderDetails["carrier-url"]=carrier_url;
		});
	}	

});
await driver.findElement(By.xpath('//div[contains(@class, "order-history-item__status-column")]//a[.="Track Item"]')).getAttribute('href').then(function (carrier_url) {
	off5thOrderDetails["carrier-url"]=carrier_url;

	if((off5thOrderDetails["order-status"].includes('Cancelled')||off5thOrderDetails["order-status"].includes('Shipped')||off5thOrderDetails["order-status"].includes('Returned'))){
	
	   //saving to DB
		insertDocument(off5thOrderDetails);
		}
	off5thOrderDetails={};
		
});
 driver.close();

}


function insertDocument(off5thOrderDetails)
{
		var MongoClient = require('mongodb').MongoClient;
		var url = 'mongodb://localhost:27017/';
		MongoClient.connect(url, function(err, db) {
			if (err) {
				console.log('problem while connection to server'+err);
			}
			var dbs=db.db('Order_BOT');
			dbs.createCollection("scrap");//creating collection if not created

					dbs.collection('scrap').insertOne(off5thOrderDetails,function(err,response){
						if(err) {
							console.log('problem while inserting data to db'+err);
						}
						console.log('inserted into db');
					}); 
					db.close();	
     });	
	}

module.exports.scrap=scrap;

