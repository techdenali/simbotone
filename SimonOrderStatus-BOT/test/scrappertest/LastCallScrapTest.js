"use strict";

const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;

const assertError=assert.AssertionError;

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/Order_BOT";

var beforeJSON = [{		 
		  carrier_url: 'http://www.fedex.com/Tracking?action=track&tracknumbers=9274897641728920631731',
		  order_date: '2017-10-30',
		  ['retailer-order-id']: 'LC1865228830',
		  product_name: 'CASHMERE SOCKS',
		  order_status: 'Shipped',
		  tracking_number: '9274897641728920631731',
		  product_amount: '1 x $11.70',
		  shipping_amount: '$5.00',
		  tax_amount: '$0.00',
		  seller_id: '100001',
		  SPO_order_id: '20000008',
		  SPO_order_line_id: '3008',
		  product_id: '10000007' 
}, {		 
		  carrier_url: 'http://www.fedex.com/Tracking?action=track&tracknumbers=9274897641728920631731',
		  order_date: '2017-10-30',
		  ['retailer-order-id']: 'LC1865228831',
		  product_name: 'CASHMERE SOCKS',
		  order_status: 'Shipped',
		  tracking_number: '9274897641728920631731',
		  product_amount: '1 x $11.70',
		  shipping_amount: '$5.00',
		  tax_amount: '$0.00',
		  seller_id: '100001',
		  SPO_order_id: '20000008',
		  SPO_order_line_id: '3008',
		  product_id: '10000007' 
}];

describe("scrapperTest", function () {	
	describe("test for matching scrap data" , function(){		
		it('Valid test', function(done){
			MongoClient.connect(url, function(err, db) {
			console.log("db connected");
			
			db.collection("scrap").findOne({"retailer-order-id":'LC1865228830'},function(err, result) {
console.log("record find "+result["retailer-order-id"]);  
				
				
				console.log("beforeJSON find "+beforeJSON[0]["retailer-order-id"]);
				//expect(result.retailer_order_id).to.equal(beforeJSON.retailer_order_id);
				assert.equal(result["retailer-order-id"],beforeJSON[0]["retailer-order-id"], "test message");
                assert.equal(null, err);
      				db.close();
      				
				done();
				
				
			});					
		});			
		});
		
		it('Invalid test', function(done){
			MongoClient.connect(url, function(err, db) {
			console.log("db connected");
			
			db.collection("scrap").findOne({"retailer-order-id":'LC1865228830'},function(err, result) {
				
				//console.log(result);
				console.log("record find "+result["retailer-order-id"]);  
				
				
				console.log("beforeJSON find "+beforeJSON[1]["retailer-order-id"]);
				//expect(result.retailer_order_id).to.equal(beforeJSON.retailer_order_id);
				assert.equal(result["retailer-order-id"],beforeJSON[1]["retailer-order-id"], "test message");
                assert.equal(null, err);
      				db.close();
      				
				done();
				
				
			});					
		});			
		});
	});	
});